@component('mail::message')
# Здравствуйте!
##  [Обращения с сайта]({{ $_SERVER['SERVER_NAME'] }})

---

>- id
>Имя
>>Телефон
>>>Обращение

---

@foreach($feedback as $appeal)

>* {{ $appeal->id }}
>{{ $appeal->user_name }}
>>{{ $appeal->user_phone }}
>>>{{ $appeal->user_comment }}

***
@endforeach


@component('mail::button', ['url' => Request::root()])
На сайт
@endcomponent


>С уважением, - Денис Белоцерковец.
@endcomponent
