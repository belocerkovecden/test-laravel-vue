@extends('layouts.app', ['title' => 'Форма для связи'])

@section('content')

    <div class="container">
        <router-view :old="{{ json_encode(Session::getOldInput()) }}"></router-view>
    </div>

@endsection

