<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

---

# Для запуска проекта
### Команды расположены сверху вниз, по частотности и важности выполнения

- Создание зависимостей php и Очистка кэша проекта  
  `composer install`  
  `composer dumpautoload -o`  
  `php artisan config:cache && php artisan route:cache && php artisan cache:clear && php artisan view:clear && php artisan config:clear`


- Создать зависимости js  
  `npm i`  
 
 
- Создать таблицы в БД, с помощью миграций, заполнив файл .env перед этим  
    `php artisan migrate`
  

- Создание символьной ссылки в директорию /storage/app   
  `php artisan storage:link`


>Linux Ubuntu
- Дать разрешения к директориям  
  `sudo chown -R $USER:$USER /var/www/html/public`  
  `chmod 755 project/`  
  `chmod 755 project/public/`  
  `chmod 644 project/public/index.php`  
  `sudo apachectl -t -D DUMP_MODULES | grep rewrite`


- Крайний случай  
  `chmod -R 777 storage`  
  `chmod -R 777 bootstrap/cache`  
  `chmod -R 0777 storage/`


- Для разработки  
  `php artisan serve`  
  `npm run watch`

>Для отправки на Ваш E-mail, укажите свой E-mail в файле app/Http/Controllers/FeedbackController.php метод `mail_send()` переменная `$toEmail` т.е. строка 104

- Собрать и минифицировать js и css  
  `npm run prod`
  
  
- Создать критический и ассинхронный css файлы  
  `node critical.js`
  

- Для создания файла smart-grid.less используй команду  
  `node smartgrid.js`


---
#### Дамп mysql проекта, находится в корне проекта ./db.sql
