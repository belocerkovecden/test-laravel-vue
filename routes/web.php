<?php

use App\Http\Controllers\FeedbackController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{any}', function () {
    return view('index');
})->where('any', '.*');


Route::post('/feedback_form', [FeedbackController::class, 'write_in_mysql'])->name('feedback_form');

// Clear cache
// Command for terminal -
// php artisan config:cache && php artisan route:cache && php artisan cache:clear && php artisan view:clear && php artisan config:clear
Route::get('/clear', function() {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    return "Кэш очищен.";
});

Auth::routes();
