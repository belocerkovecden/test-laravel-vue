<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedbackForm extends Model
{
    use HasFactory;

    protected $fillable = ['user_name', 'user_phone', 'user_comment'];

    public function feedback()
    {
        return $this->belongsTo(FeedbackForm::class);
    }
}
