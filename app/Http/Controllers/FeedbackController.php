<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FeedbackForm;
use Illuminate\Support\Facades\Storage;
use App\Mail\SendingMail;

class FeedbackController extends Controller
{
    private function factoryData($request)
    {
        $request->validate([
            'user_name' => 'required|min:3|max:100',
            'user_phone' => 'required|min:5|max:30',
            'user_comment' => 'required|min:3|max:200',
        ]);

        return $request;
    }

    /*
     * Пишем в файл storage/app/public/yandexmarket.xml
     * */
    public function write_in_xml()
    {
        $appeals = FeedbackForm::all();

        /* Формирование листа YML */
        $str = '';
        foreach ($appeals as $appeal) {
            $str .= '<offer>';
            $str .= '<id>';
            $str .= $appeal->id;
            $str .= '</id>' . PHP_EOL;
            $str .= '<user_name>';
            $str .= $appeal->user_name;
            $str .= '</user_name>' . PHP_EOL;
            $str .= '<user_phone>';
            $str .= $appeal->user_phone;
            $str .= '</user_phone>' . PHP_EOL;
            $str .= '<user_comment>';
            $str .= $appeal->user_comment;
            $str .= '</user_comment>' . PHP_EOL;
            $str .= '<create_at>';
            $str .= $appeal->create_at;
            $str .= '</create_at>' . PHP_EOL;
            $str .= '<updated_at>';
            $str .= $appeal->updated_at;
            $str .= '</updated_at>' . PHP_EOL;
            $str .= '<manufacturer_warranty>true</manufacturer_warranty>' . PHP_EOL;
            $str .= '</offer>' . PHP_EOL;
        }

        $new_date = date("Y-m-d H:i", time() + (60 * 60 * 3));

        $yml = '<?xml version="1.0" encoding="utf-8"?>
        <!DOCTYPE yml_catalog SYSTEM "shops.dtd">
        <yml_catalog date="' . $new_date . '">
            <shop>
        <name>Данные из таблицы MySQL | Данные из таблицы MySQL</name>
        <company>TEST</company>
        <url>https://TEST/</url>
        <phone>+7 (928) 304-85-94</phone>
        <platform>Laravel</platform>
        <version>Laravel Framework 8</version>
        <currencies>
            <currency id="RUB" rate="1"/>
        </currencies>
        <categories>
            <category id="1">Обращения</category>
        </categories>
        <delivery-options>
            <option cost="0" days="" order-before="24"/>
        </delivery-options>
        <offers>' . $str . '</offers>' . PHP_EOL . '</shop>' . PHP_EOL . '</yml_catalog>';
        /* END Формирование листа YML */

        if (Storage::exists('yandexmarket.xml')) {
            Storage::put('yandexmarket.xml', $yml);
        }
    }

    /*
     * Пишем в БД MySQL
     * */
    public function write_in_mysql(Request $request)
    {
        $request = $this->factoryData($request);
        FeedbackForm::create($request->all());
        //$this->write_in_xml();
        //$this->mail_send();
    }

    /*
     * Отправляем на E-mail
     * */
    public function mail_send()
    {
        $appeals = FeedbackForm::all();
        $toEmail = "belocerkovecden1@ya.ru";
        Mail::to($toEmail)->send(new SendingMail($appeals));
        return 'Сообщение отправлено на адрес '. $toEmail;
    }
}
